<?php
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'config.php';

if (empty($_SESSION['user'])) {
    header('Location: index.php');
    die();
}

if (!empty($_POST['products'])) {
    if (empty($_SESSION['cart_id'])) {
        $cartId = createCart($pdo, $_SESSION['user']['id']);
        debug($cartId);
        if (!empty($cartId)) {
            $_SESSION['cart_id'] = $cartId;
        }
    }
    addProductsToCart($pdo, $_SESSION['cart_id'], $_POST['products']);
}

if (empty($_SESSION['cart_id'])) {
    header("Location: index.php");
    die();
}

$selectedProducts = getProductsFromCart($pdo, $_SESSION['cart_id']);
if (!empty($_POST['products'])){
    if (isset($_POST['del_from_cart'])) {
        $delProductsId = $_POST['products'];
        foreach ($delProductsId as $delProductId) {
            if (deleteProdFromCart($pdo, $delProductId)) {
                header("Location: cart.php");
                die();
            }
        }
    }
}

require_once ROOT_PATH . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'cart.php';

