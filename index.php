<?php require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'config.php';

if (empty($_SESSION['user'])) {
    truncateCartProduct($pdo);
    header('Location: login.php');
    die();
}


if (!empty($_GET['cat_id'])) {
    $products = getCategoryId($pdo, (int)$_GET['cat_id']);
} else {
    $products = getProducts($pdo);
}
$categories = getCategories($pdo);


require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'shop.php';
