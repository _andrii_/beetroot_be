<?php

function debug($val)
{
    echo '<pre>' . print_r($val, 1) . '</pre>';
}

function getProducts(object $pdo): array
{
    if (empty($pdo)) {
        return [];
    }

    $stmt = $pdo->prepare("SELECT * FROM `products`");
    $stmt->execute([]);
    return !($products = $stmt->fetchAll()) ? [] : $products;
}

function getCategories(object $pdo): array
{
    if (empty($pdo)) {
        return [];
    }

    $stmt = $pdo->prepare("SELECT * FROM `categories`");
    $stmt->execute([]);
    return !($categories = $stmt->fetchAll()) ? [] : $categories;
}

function getCategoryId(object $pdo, $categoryId): array
{
    if (empty($pdo) || empty($categoryId)) {
        return [];
    }
    $smtp = $pdo->prepare(
        "
        SELECT
            `products`.*
        FROM
            `products` 
        INNER JOIN `categories` ON `categories`.`id` = `products`.`category_id`
        WHERE
            `category_id` = :category_id
    "
    );
    $smtp->execute(["category_id" => $categoryId]);
    return $smtp->fetchAll();
}

function createCart(object $pdo, string $userId): int
{
    if (empty($pdo) || empty($userId)) {
        return 0;
    }
    $smtp = $pdo->prepare(
        "INSERT INTO `cart`(`user_id`) 
         VALUES( :user_id )"
    );
    $smtp->execute(["user_id" => $userId]);
    return $pdo->lastInsertId();
}

function addProductsToCart(object $pdo, int $cartId, array $products): bool
{
    if (empty($pdo) || empty($cartId) || empty($products)) {
        return false;
    }
    foreach ($products as $product) {
        $isProductAdded = getProductFromCart($pdo, $cartId, $product);
        if (!empty($isProductAdded)) {
            //update product quantity
        } else {
            $smtp = $pdo->prepare("
                INSERT INTO `cart_products` (
                    `cart_id`,
                    `product_id`,
                    `quantity`
                )
                VALUES
                (
                    :cart_id,
                    :product_id,
                    :quantity
                )");
            $smtp->execute([
                               "cart_id" => $cartId,
                               "product_id" => $product,
                               "quantity" => 1
                           ]);
        }
    }
    return true;
}


function getProductFromCart(object $pdo, int $cartId, int $productId): array
{
    if (empty($pdo) || empty($cartId) || empty($productId)) {
        return [];
    }
    $stmt = $pdo->prepare(
        "
        SELECT
            *
        FROM
            `cart_products`
        WHERE
            `cart_id` = :cart_id
        AND `product_id` = :product_id
    "
    );
    $stmt->execute(
        [
            "cart_id" => $cartId,
            "product_id" => $productId
        ]
    );
    return !($product = $stmt->fetch()) ? [] : $product;
}

function getProductsFromCart(object $pdo, int $cartId): array
{
    if (empty($pdo) || empty($cartId)) {
        return [];
    }
    $smtp = $pdo->prepare(
        "
        SELECT
            `products`.*,
            `cart_products`.`quantity` as selected_quantity
        FROM
            `cart_products`
        INNER JOIN `products` ON `products`.`id` = `cart_products`.`product_id`
        WHERE
            `cart_id` = :cart_id
    "
    );
    $smtp->execute(
        [
            "cart_id" => $cartId,
        ]
    );
    return !($products = $smtp->fetchAll()) ? [] : $products;
}

function deleteProdFromCart(object $pdo,  int $delProductId): bool
{

    $stmt = $pdo->prepare( "DELETE FROM `cart_products` 
                            WHERE `cart_products`.`product_id` = :product_id");
    $stmt->execute(['product_id' => $delProductId]);
    return true;
}

function truncateCartProduct($pdo)
{
    $pdo->exec("TRUNCATE TABLE `cart_products`");
}

function checkEmail(object $pdo, string $email): bool
{
    $stmt = $pdo->prepare(
        "
        SELECT `email` 
        FROM `users` 
        WHERE `email` = :email"
    );
    $stmt->execute(['email' => $email]);
    return (bool)$stmt->fetch();
}

function checkLogin(object $pdo, string $email, string $password): array
{
    if (empty($pdo) || empty($email) || empty($password)) {
        return [];
    }

    $stmt = $pdo->prepare(
        "
        SELECT
            `users`.*
        FROM
            `users`
        WHERE
            `email` = :email
    "
    );
    $stmt->execute(["email" => $email]);
    $user = $stmt->fetch();
    if (password_verify($password, $user['password'])) {
        return $user;
    }
    return [];
}

function userRegistration(object $pdo, string $name, string $email, string $password): string
{
    $stmt = $pdo->prepare(
        "INSERT INTO `users` (
                     `name`, 
                     `email`, 
                     `password`, 
                     `status`) 
    VALUES (
            :name, 
            :email, 
            :password, 
            :status)"
    );
    $stmt->execute(
        [
            'name' => $name,
            'email' => $email,
            'password' => crypt($password, SALT),
            'status' => 'Active'
        ]
    );
    return $pdo->lastInsertId();
}

function userById(object $pdo, int $userId): array
{
    if (empty($pdo) || empty($userId)) {
        return [];
    }
    $smtp = $pdo->prepare("SELECT * FROM `users` WHERE `id` = :id");
    $smtp->execute(["id" => $userId]);
    return !($user = $smtp->fetch()) ? [] : $user;
}

