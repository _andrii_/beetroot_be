<?php

define('ROOT_PATH' , dirname(__FILE__));
const DB_USER = 'user_db';
const DB_PASS = '1111';
const DB_NAME = 'test_db';
const SALT = 'l2djf-0U?n?OL*654#b#UR_F34dYk2e56vbu4j6@-^#wddfgKJsd345';

require_once ROOT_PATH . DIRECTORY_SEPARATOR . "functions.php";

session_start();

$dsn = "mysql:host=localhost;port=3306;dbname=" . DB_NAME . ";charset=utf8";

$options = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
];

$pdo = new PDO($dsn, DB_USER, DB_PASS, $options);
