/*
 Navicat Premium Data Transfer

 Source Server         : test
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : test_db

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 23/07/2021 08:48:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int UNSIGNED NOT NULL,
  `ordered_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cart
-- ----------------------------

-- ----------------------------
-- Table structure for cart_products
-- ----------------------------
DROP TABLE IF EXISTS `cart_products`;
CREATE TABLE `cart_products`  (
  `cart_id` int UNSIGNED NOT NULL,
  `product_id` int UNSIGNED NOT NULL,
  `quantity` int UNSIGNED NOT NULL,
  PRIMARY KEY (`cart_id`, `product_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cart_products
-- ----------------------------
INSERT INTO `cart_products` VALUES (6, 1, 1);
INSERT INTO `cart_products` VALUES (6, 2, 1);
INSERT INTO `cart_products` VALUES (6, 3, 1);
INSERT INTO `cart_products` VALUES (6, 11, 1);
INSERT INTO `cart_products` VALUES (6, 12, 1);

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 'Samsung');
INSERT INTO `categories` VALUES (2, 'Xioami');
INSERT INTO `categories` VALUES (3, 'LG');
INSERT INTO `categories` VALUES (4, 'Philips');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `price` decimal(8, 2) UNSIGNED NOT NULL,
  `qty` int NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, 1, 'Samsung Note', 'Lorem ipsum, lorem lorem', 8000.00, 44, '2021-07-12 08:19:06', '2021-07-12 08:19:06');
INSERT INTO `products` VALUES (2, 2, 'Xiaomi Phone', 'Lorem ipsum, lorem lorem', 5000.00, 40, '2021-07-12 08:20:06', '2021-07-12 08:20:06');
INSERT INTO `products` VALUES (3, 2, 'Xiaomi Redmi', 'Lorem ipsum, lorem lorem', 4000.00, 70, '2021-07-12 08:21:09', '2021-07-12 08:21:09');
INSERT INTO `products` VALUES (4, 4, 'Philips 12', 'Lorem ipsum, lorem lorem', 9000.00, 66, '2021-07-12 08:23:30', '2021-07-12 08:23:30');
INSERT INTO `products` VALUES (5, 4, 'Philips X', 'Lorem ipsum, lorem lorem', 8999.00, 49, '2021-07-12 08:34:26', '2021-07-12 08:34:26');
INSERT INTO `products` VALUES (6, 2, 'Xiaomi Redmi Note', 'Lorem loremlorem, ipsum', 8000.00, 62, '2021-07-12 08:34:26', '2021-07-12 08:34:26');
INSERT INTO `products` VALUES (7, 4, 'Philips 12 Max Pro', 'Lorem ipsum, lorem lorem', 29999.00, 75, '2021-07-12 09:59:17', '2021-07-12 09:59:17');
INSERT INTO `products` VALUES (8, 1, 'Samsung Phone', 'lorem lorem', 8000.00, 50, '2021-07-15 13:01:04', '2021-07-15 13:01:04');
INSERT INTO `products` VALUES (9, 2, 'Xiaomi Redmi 11', 'lorem ipsum', 5000.00, 60, '2021-07-15 13:01:04', '2021-07-15 13:01:04');
INSERT INTO `products` VALUES (10, 3, 'LG G10', 'Ipsum lorem', 6000.00, 60, '2021-07-15 13:01:04', '2021-07-15 13:01:04');
INSERT INTO `products` VALUES (11, 4, 'Philips Phone', 'Ipsum ipsum', 2000.00, 50, '2021-07-15 13:01:04', '2021-07-15 13:01:04');
INSERT INTO `products` VALUES (12, 1, 'Samsung Note', 'lorem ipsum, lorem', 12000.00, 90, '2021-07-15 13:01:04', '2021-07-15 13:01:04');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `login` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` enum('Active','Not active','Locked') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'Not active',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (4, 'Andrii', NULL, 'test@test.loc', 'ldyqZB9f/d6Ew', 'Active', '2021-07-21 19:09:28');
INSERT INTO `users` VALUES (5, 'user', NULL, 'user@test.loc', 'ld8v432xC0k6A', 'Active', '2021-07-21 20:17:05');
INSERT INTO `users` VALUES (6, 'admin', NULL, 'admin@t.loc', 'ld0cHlLaRk5Ow', 'Active', '2021-07-22 20:53:10');

SET FOREIGN_KEY_CHECKS = 1;
