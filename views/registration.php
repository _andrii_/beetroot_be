<?php require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'partials' . DIRECTORY_SEPARATOR . 'header.php'; ?>

<main class="form-signin">
    <?php
    if (!empty($errors)) : ?>
        <div class="alert alert-danger" role="alert">
            <?php
            foreach ($errors as $error): ?>
                <span><?php
                    echo $error; ?></span><br/>
            <?php
            endforeach; ?>
        </div>
    <?php
    endif; ?>
    <form method="post" action="/registration.php">
        <h1 class="h3 mb-3 fw-normal">Please sign up</h1>
        <div class="form-floating">
            <input type="text" class="form-control" id="floatingInput" name="name" placeholder="Name" value="">
            <label for="floatingInput">Name</label>
        </div>
        <div class="form-floating">
            <input type="email" class="form-control" id="floatingInput" name="email" placeholder="email" value="">
            <label for="floatingInput">Email</label>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" id="floatingPassword" name="password"
                   placeholder="password" value="">
            <label for="floatingPassword">Password</label>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" id="floatingPasswordConfirm" name="password_confirm"
                   placeholder="Password Confirm" value="">
            <label for="floatingPasswordConfirm">Password Confirm</label>
        </div>
        <button class="w-100 btn btn-lg btn-primary" type="submit">Sign up</button>
    </form>
</main>


<?php require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'partials' . DIRECTORY_SEPARATOR . 'footer.php'; ?>
