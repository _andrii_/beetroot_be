<?php
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'config.php';

unset($_SESSION['user']);
header('Location: index.php');