<?php


class Driver extends Worker
{
    private int $expirience;
    private string $categories;

    public function __construct(string $name, int $age, int $salary, int $expirience, string $categories)
    {
        parent::__construct($name, $age, $salary);
        $this->expirience = $expirience;
        $this->categories = $categories;
    }

    public function getExpirience(): int
    {
        return $this->expirience;
    }

    public function setExpirience($expirience)
    {
        $this->expirience = $expirience;
        return $this;
    }

    public function getCategories(): string
    {
        return $this->categories;
    }

    public function setCategory($categories)
    {
        $this->categories = $categories;
        return $this;
    }
}