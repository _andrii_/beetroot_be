<?php
define('ROOT_PATH', dirname(__FILE__));
const CLASSES = ROOT_PATH . DIRECTORY_SEPARATOR . 'classes';

require_once ROOT_PATH . DIRECTORY_SEPARATOR . 'functions.php';
require_once CLASSES . DIRECTORY_SEPARATOR . 'User.php';
require_once CLASSES . DIRECTORY_SEPARATOR . 'Worker.php';
require_once CLASSES . DIRECTORY_SEPARATOR . 'Student.php';
require_once CLASSES . DIRECTORY_SEPARATOR . 'Driver.php';