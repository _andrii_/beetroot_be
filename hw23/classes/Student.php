<?php


class Student extends User
{
    private int $stipend;
    private int $course;


    public function __construct(string $name, int $age, int $stipend, int $course)
    {
        parent::__construct($name, $age);
        $this->stipend = $stipend;
        $this->course = $course;
    }


    public function getStipend(): int
    {
        return $this->stipend;
    }

    public function setStipend($stipend)
    {
        $this->stipend = $stipend;
        return $this;
    }

    public function getCourse(): int
    {
        return $this->course;
    }

    public function setCourse($course)
    {
        $this->course = $course;
        return $this;
    }

}