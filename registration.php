<?php

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'config.php';

if (!empty($_SESSION['user'])) {
    header('Location: index.php');
    die();
}


$fields = ["name", "email", "password", "password_confirm"];
if (!empty($_POST)) {
    $errors = [];
    foreach ($fields as $field) {
        if (empty($_POST[$field])) {
            $errors[] = "Field " . ucwords(str_replace("_", " ", $field)) . " is required";
        }
    }

    if (!empty($_POST['email']) && checkEmail($pdo, $_POST['email'])) {
        $errors[] = "Email must be unique";
    }

    if ($_POST['password'] !== $_POST['password_confirm']) {
        $errors[] = "Password must be confirmed";
    }

    if (empty($errors)) {
        $userId = userRegistration($pdo, trim($_POST['name']), trim($_POST['email']), trim($_POST['password']));
        $_SESSION['user_id'] = userById($pdo, $userId);
        header('Location: index.php');
        die();
    }
}

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'registration.php';