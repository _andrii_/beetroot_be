<?php
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'partials' . DIRECTORY_SEPARATOR . 'header.php'; ?>

<main class="form-signin">
    <?php
    if (!empty($error)) : ?>
        <div class="alert alert-danger" role="alert">
            <?php
            echo $error; ?>
        </div>
    <?php
    endif; ?>
    <form method="post" action="/login.php">
        <h1 class="h3 mb-3 fw-normal">Please sign in</h1>

        <div class="form-floating">
            <input type="email" class="form-control" id="floatingInput" name="email" placeholder="email" value="">
            <label for="floatingInput">Email</label>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" id="floatingPassword" name="password"
                   placeholder="password" value="">
            <label for="floatingPassword">Password</label>
        </div>

        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" name="remember_me" value="remember-me"> Remember me
            </label>
        </div>
        <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
    </form>
    <div class="col">
        <p class="text-center">Or <br>
            <a href="/registration.php">Registration</a>
        </p>

    </div>
</main>


<?php
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'partials' . DIRECTORY_SEPARATOR . 'footer.php'; ?>
